package examples.dao;

import org.jeecgframework.minidao.annotation.MiniDao;
import org.jeecgframework.minidao.annotation.Sql;
import org.jeecgframework.minidao.hibernate.MiniDaoSupportHiber;

import examples.entity.Student;

@MiniDao
public interface StudentDao extends MiniDaoSupportHiber<Student> {
	@Sql("SELECT count(*) FROM user")
	Integer getCount();
}
