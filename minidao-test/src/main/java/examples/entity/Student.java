package examples.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.jeecgframework.minidao.hibernate.IdEntity;

@Entity
@Table(name = "student")
@Inheritance(strategy = InheritanceType.JOINED)
public class Student extends IdEntity implements Serializable {

	private static final long serialVersionUID = 2L;

	private String name;
	private String sex;
	private String salary;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

}
