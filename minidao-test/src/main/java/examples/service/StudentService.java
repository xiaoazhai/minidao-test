package examples.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import examples.dao.StudentDao;
import examples.entity.Student;

@Service
public class StudentService {

	@Autowired
	StudentDao studentDao;

	public void saveByHiber(Student student) {
		studentDao.saveByHiber(student);
	}

	public Student getByIdHiber(Class<Student> class1, String id) {
		return studentDao.getByIdHiber(class1, id);
	}
}
