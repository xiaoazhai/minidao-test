package test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.spring.SpringTxTestCase;
import examples.dao.JeecgDemoDao;
import examples.entity.Student;
import examples.service.StudentService;

public class Client extends SpringTxTestCase {

	@Autowired
	private StudentService studentService;
	@Autowired
	private JeecgDemoDao jeecgDemoDao;

	// @Test
	public void insertStudent() {
		Student student = new Student();
		student.setName("柳杨");
		student.setSex("男");
		student.setSalary("3000");
		studentService.saveByHiber(student);
		// JeecgDemo jeecgDemo = new JeecgDemo();
		// jeecgDemo.setUserName("柳杨");
		// jeecgDemoDao.saveByHiber(jeecgDemo);
	}

	@Test
	public void testStudent() {
		Student student = studentService.getByIdHiber(Student.class, "1");
		System.out.println("---------------------" + student.getName() + "----------------");
	}

	// public static void main(String[] args) {
	//
	// BeanFactory factory = new
	// ClassPathXmlApplicationContext("applicationContext.xml");
	// StudentDao studentDao = (StudentDao) factory.getBean("studentDao");
	// List<Map<String, Object>> list = studentDao.getAllStudents();
	// System.out.println(list);
	// System.out.println("complete");
	// }
}
